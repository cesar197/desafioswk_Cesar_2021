import React from 'react';
import axios from 'axios';
import Obter from './Components/Obter';
import InputFields from './Components/InputsFields';
import Iframeinouts from './Components/Iframeinputs';
import './App.css';

class App extends React.Component {
  constructor() {
    super()
    this.state = {
      backendStatus: '...'
    };
  }

  componentDidMount() {
    axios({method: 'get', url: 'http://localhost:796'})
    .then(() => this.setState({backendStatus: 'OK'}))
    .catch(() => this.setState({backendStatus: 'BAD'}));
  }

  render() {
    return (
      <div className="App">
     
       <InputFields></InputFields>
       <br/>
        <div className="ListarVideos">
        <Obter></Obter>
        </div>
        <Iframeinouts></Iframeinouts>
      </div>
    );
  }
}

export default App;
