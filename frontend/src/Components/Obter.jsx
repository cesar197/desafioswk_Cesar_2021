import React from 'react';



let Parser = require('rss-parser');
let parser = new Parser();
const CORS_PROXY = "https://cors-anywhere.herokuapp.com/";



class Feed extends React.Component{
  constructor(props) {
    super(props);
    this.state = { 
      feed: []
     };
  }

  async componentDidMount() {
      //Buscar o xml do video que depois ira ser buscada por backend
    const feed = await parser.parseURL(CORS_PROXY + 'https://www.youtube.com/feeds/videos.xml?channel_id=UC-lHJZR3Gqxm24_Vd_AJ5Yw');
    this.setState(feed)
    console.log(feed);
  }

  render() {
    return (
    <div>      
      
      <h1>Videos</h1>
      
     
      {this.state.items && this.state.items.map((items, i) => (
                <div className="videos" key={i}>
                    <iframe
                    src={'' + items.link.replace("www.youtube.com/watch?v=","www.youtube.com/embed/")}>
                    </iframe> 
                    <p>{items.title}</p>               
                                       
                </div>
      ))}
        <br />
    </div>
    );
  }
}
export default props =>{
  return(
      <React.Fragment>
          <Feed></Feed>
      </React.Fragment>
  )
}