<?php

namespace App\Http\Controllers;

use App\videos;
use Illuminate\Http\Request;

class videoctrl extends Controller
{

    public function showAllAuthors()
    {
        return response()->json(videos::all());
    }

    public function showOneAuthor($id)
    {
        return response()->json(videos::find($id));
    }

    public function create(Request $request)
    {
        $videos = videos::create($request->all());

        return response()->json($videos, 201);
    }

    public function update($id, Request $request)
    {
        $videos = videos::findOrFail($id);
        $videos->update($request->all());

        return response()->json($videos, 200);
    }

    public function delete($id)
    {
        videos::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }
   //buscar xml do youtube para depois fazer a base de dados atravez do channel id
    public function getvideos($urldosite)
    {
        
       
        $url = "https://www.youtube.com/feeds/videos.xml?channel_id=".$urldosite;
        $xml = simplexml_load_file($url);
        $teste = $xml->entry[1]->title;
       
       
        return response()->json($xml);
    }
   


    
}