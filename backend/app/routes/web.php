<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get(
    '/',
    function () use ($router) {
        return "Hello from swk dev team";
    }
);

$router->group(['prefix' => 'api'], function () use ($router) {
    $router->get('videos',  ['uses' => 'videoctrl@showAllAuthors']);
  
    $router->get('videos/{id}', ['uses' => 'videoctrl@showOneAuthor']);
  
    $router->post('videos', ['uses' => 'videoctrl@create']);
  
    $router->delete('videos/{id}', ['uses' => 'videoctrl@delete']);
  
    $router->put('videos/{id}', ['uses' => 'videoctrl@update']);

    $router->get('videos/fetch/{urldosite}', ['uses' => 'videoctrl@getvideos']);
  });