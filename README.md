# Welcome to Swonkie Challenge

In order to accomplish the task presented bellow you will need the following tools:

 - A cool and functional Code Editor or IDE (we like VSCode, but it's your choice)
 - Docker: [Windows](https://desktop.docker.com/win/stable/Docker%20Desktop%20Installer.exe) ([WSL2](https://docs.microsoft.com/en-us/windows/wsl/install-win10) installation, you will need it), [Mac OS](https://desktop.docker.com/mac/stable/Docker.dmg) and [Linux](https://docs.docker.com/engine/install/ubuntu/)
 - Git: [Windows](https://git-scm.com/download/win), [Mac OS](https://git-scm.com/download/mac) and [Linux](https://git-scm.com/download/linux)
 - Kind: [Installation Guide](https://kind.sigs.k8s.io/docs/user/quick-start/#installation)
 - Kubernetes: [Installation Guide](https://kubernetes.io/docs/tasks/tools/install-kubectl/)

After having everything setup, clone this repo to your local machine and run the command:

> docker-compose up --build -d

This will create three containers named "swk-challenge-frontend", "swk-challenge-backend" and "swk-challenge-database", after the container creation you should be able to access the service via [localhost:795](http://localhost:795), if the returns an error maybe the container isn't fully started yet, check the logs with:

> docker logs -f swk-challenge-frontend

Once it starts this [localhost:795](http://localhost:795) should display `Hello from swk dev team, backend status: OK`. If backend status is `BAD`, check backend logs with:

> docker logs -f swk-challenge-backend

Backend listen on `796`, this means you can request to it via [localhost:796](http://localhost:796), it should also display a message `Hello from swk dev team`.

# Challenge
This is where the fun starts 💻🥳

The challenge here is to build a web app that lists a Youtube channel's videos and allows the user to click the video and watch it.

This as to be made using:
 - React
 - SCSS
 - PHP
 - Lumen
 - Laravel Migrations
 - Eloquent
 - Mysql

The following features are required:

 - Input to insert the channel url the user wants to list videos from
   - This will send a request to backend
   - The backend will read the url, get the videos list and save it in the database
 - List the videos from the database ordered by video data in a side panel
   - Each row needs to have a trash icon that deletes the video from the database when clicked
 - When a video row is clicked play it in an iframe filling the rest of the window

The layout is your choice, make it pretty.

Once the frontend, backend and database are completed, create a deployment file that deploys a replica for each service, the goal here is to deploy all the services inside a kubernetes cluster, we should be able to do this with the deployment file by running a single command:

> kubectl apply -f ./deployment.yml

Before deploy normally you need to build the container images like this:

> docker-compose build

# Helpful Links
 - https://reactjs.org/tutorial/tutorial.html
 - https://lumen.laravel.com/docs/8.x
 - https://laravel.com/docs/8.x/migrations
 - https://laravel.com/docs/8.x/eloquent
 - https://danielmiessler.com/blog/rss-feed-youtube-channel/
 - https://developers.google.com/youtube/player_parameters
 - https://kubernetes.io/docs/concepts/workloads/controllers/deployment/

# Tips

## Docker on windows
In windows you should have your projects inside the WSL environment to do this you can open the CMD and type:

> wsl

Then you will enter WSL environment, navigate to you user dir and created a dir named `projects`:

> cd ~
>
> mkdir projects
>
> cd projects

Now you can clone this repo to your dir:

> git clone git@gitlab.com:example-repo.git

## Install react packages
To install packages with this setup you need to send the command to be process inside the container, like this:

> docker exec -it swk-challenge-frontend npm install --save example-package

Some times react dev environment doesn't detect the new package automatically, in those cases a quick restart should fix it:

> docker restart swk-challenge-frontend

## How to use `php artisan`
To use php artisan you must be in the container command line the the base directory of your lumen app,  to do that run the following command:

> docker exec -it swk-challenge-backend bash

**Note:** Now that you are in the container command line you should be able to use `php artisan`